const expect = require('chai').expect;
const requestValidation = require('../../app/lib/request_validation');

describe('Request Validation', function() {
  beforeEach(() => {
    request = {
      cep: '09810650',
      phone_number: '11989542304'
    };
  });

  it('given empty cep should return Cep é obrigatório, deve conter apenas números e somente 8 caracteres', () => {
    request.cep = '';

    const validationResult = requestValidation(request);
    expect(validationResult.cep).to.equal('Cep é obrigatório, deve conter apenas números e somente 8 caracteres');
  });

  it('given request without cep should return Cep é obrigatório, deve conter apenas números e somente 8 caracteres', () => {
    delete request.cep;

    const validationResult = requestValidation(request);
    expect(validationResult.cep).to.equal('Cep é obrigatório, deve conter apenas números e somente 8 caracteres');
  });

  it('given cep with letters should return Cep é obrigatório, deve conter apenas números e somente 8 caracteres', () => {
    request.cep = 'qwer1234';

    const validationResult = requestValidation(request);
    expect(validationResult.cep).to.equal('Cep é obrigatório, deve conter apenas números e somente 8 caracteres');
  });

  it('given cep with 9 length should return Cep é obrigatório, deve conter apenas números e somente 8 caracteres', () => {
    request.cep = '123456789';

    const validationResult = requestValidation(request);
    expect(validationResult.cep).to.equal('Cep é obrigatório, deve conter apenas números e somente 8 caracteres');
  });

  it('given valid cep, cep error should be empty', () => {
    const validationResult = requestValidation(request);
    expect(validationResult.cep).to.be.empty; // jshint ignore:line
  });

  it('given empty phone_number should return Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.', () => {
    request.phone_number = '';

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.equal('Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.');
  });

  it('given request without phone_number should return Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.', () => {
    delete request.phone_number;

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.equal('Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.');
  });

  it('given phone_number with letters should return Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.', () => {
    request.phone_number = 'qwer1234';

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.equal('Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.');
  });

  it('given phone_number with 9 length should return Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.', () => {
    request.phone_number = '123456789';

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.equal('Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.');
  });

  it('given phone_number with 12 length should return Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.', () => {
    request.phone_number = '123456789224';

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.equal('Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.');
  });

  it('given valid phone_number with 11 length, phone_number error should be empty', () => {
    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.be.empty; // jshint ignore:line
  });

  it('given valid phone_number with 10 length, phone_number error should be empty', () => {
    request.phone_number = '1140763314';

    const validationResult = requestValidation(request);
    expect(validationResult.phone_number).to.be.empty; // jshint ignore:line
  });

  it('given valid request, validation result should be empty', () => {
    const validationResult = requestValidation(request);
    expect(validationResult).to.be.empty; // jshint ignore:line
  });
});
