const expect = require('chai').expect;
const parseCepServiceResult = require('../../app/services/parse_cep_service_result');

describe('Parse Cep Service Result', function() {
  beforeEach(() => {
    apiResult = {
      bairro: 'Mauá',
      cidade: 'São Caetano do Sul',
      cep: '09580750',
      logradouro: 'Rua da Paz',
      estado: 'SP'
    };

    queryResult = {
      cep_status: 1
    };
  });

  it('given bairro Mauá should return Mauá', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.bairro).to.equal('Mauá');
  });

  it('given empty bairro should return empty bairro', () => {
    apiResult.bairro = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.bairro).to.equal('');
  });

  it('given null bairro should return empty bairro', () => {
    apiResult.bairro = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.bairro).to.equal('');
  });

  it('given cidade São Caetano do Sul should return localidade São Caetano do Sul', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.localidade).to.equal('São Caetano do Sul');
  });

  it('given empty cidade should return empty localidade', () => {
    apiResult.cidade = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.localidade).to.equal('');
  });

  it('given null cidade should return empty localidade', () => {
    apiResult.cidade = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.localidade).to.equal('');
  });

  it('given cep 09580750 should return 09580750', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep).to.equal('09580750');
  });

  it('given empty cep should return empty cep', () => {
    apiResult.cep = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep).to.equal('');
  });

  it('given null cep should return empty cep', () => {
    apiResult.cep = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep).to.equal('');
  });

  it('given logradouro Rua da Paz should return Rua da Paz', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.logradouro).to.equal('Rua da Paz');
  });

  it('given empty logradouro should return empty logradouro', () => {
    apiResult.logradouro = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.logradouro).to.equal('');
  });

  it('given null logradouro should return empty logradouro', () => {
    apiResult.logradouro = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.logradouro).to.equal('');
  });

  it('given estado SP should return uf SP', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.uf).to.equal('SP');
  });

  it('given empty estado should return empty uf', () => {
    apiResult.estado = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.uf).to.equal('');
  });

  it('given null estado should return empty uf', () => {
    apiResult.estado = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.uf).to.equal('');
  });

  it('given cep_status 1 should return 1', () => {
    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep_status).to.equal(1);
  });

  it('given empty cep_status should return empty cep_status', () => {
    queryResult.cep_status = '';

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep_status).to.equal('');
  });

  it('given null cep_status should return empty cep_status', () => {
    queryResult.cep_status = null;

    const result = parseCepServiceResult(apiResult, queryResult);
    expect(result.cep_status).to.equal('');
  });
});
