const expect = require('chai').expect;
const restGetRequest = require('../../app/data/rest_get_request');

describe('REST Get Request', function() {
  beforeEach(() => {
    url = 'http://api.postmon.com.br/v1/cep/';
  });

  it('Given valid request should return correio data', (done) => {
    restGetRequest(`${url}09580750`).then((result) => {
      const expected = {
        bairro: 'Mauá',
        cidade: 'São Caetano do Sul',
        cep: '09580750',
        logradouro: 'Rua da Paz',
        estado_info: { area_km2: '248.222,362', codigo_ibge: '35', nome: 'São Paulo' },
        cidade_info: { area_km2: '15,331', codigo_ibge: '3548807' },
        estado: 'SP'
      };

      expect(expected).to.deep.equal(result);
      done();
    }).catch((error) => {
      done(error);
    });
  });

  it('Given invalid cep should return unexpected status code', (done) => {
    restGetRequest(`${url}bololohaha`).then((result) => {
      done();
    }).catch((error) => {
      expect('Unexpected status code: 404, Url: http://api.postmon.com.br/v1/cep/bololohaha').to.equal(error.error);
      done();
    });
  });
});
