### Desafio Escale

Criar uma API de busca de CEP que será integrada ao Sistema de classificação de usuários. 

### Request
```
localhost:3000/search/?cep=01544010&phone_number=1140761510
```

### Response
```
{
  "cep": "01544010",
  "logradouro": "Rua Mesquita",
  "bairro": "Vila Deodoro",
  "localidade": "São Paulo",
  "uf": "SP",
  "cep_status": 1
}
```

### Como rodar o projeto localmente

É necessário nodejs v6.9.1 e o mongodb. 
Faça download da  [base de dados](https://drive.google.com/open?id=0BwJ3cqGq1xUZNFFaWG1fLTlBV3c) (não é obrigatório importar a base para que o projeto funcione)
Importe a base de dados no mongodb
```
mongoimport --db escale-desafio --collection correios --type csv --headerline --file ~/Downloads/CEP_16_09_29.txt
```
Instale as dependencias utilizando 
```
npm install
```
Para rodar o projeto utilize 
```
npm run start
```
Para rodar os testes
```
npm run test
```
para rodar jshint
```
npm run hint
```

### Considerações

node.js interpreta as requisições de forma assíncrona em vez de sequenciais e não permite bloqueio.
Perfeito para criar APIs, aplicações web real-time como servidores de chat ou aplicações colaborativas entre múltiplos usuários, tem grandes
empresas que já utilizam como: walmart, paypal, netflix e etc. 
Algumas linguagens não tem nativamente, essa arquitetura que o node.js oferece, além disso, hoje em dia temos uma infraestrutura poderosa que 
da a possibilidade de aproveitar melhor os servidores.
Ao meu ver o node.js é a forma mais simples para lidar com concorrência, se for comparado a outras linguagens que resolvem o mesmo problema,
porém o JavaScript sofre por falta de padrão, a linguagem tem poucos recursos se for comparada a outras, tanto é que o ECMAScript vem para
melhorar essas coisas.
Se a empresa for stack Java ou .NET, ela pode resolver esse problema de concorrência sem partir para outra linguagem, o .NET mesmo 
tem async / await nativo mudando pouca coisa na implementação, é praticamente um código orientado a objeto, nada de promises ou callback.
