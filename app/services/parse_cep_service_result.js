const parse = (apiCorreiosResult, databaseResult) => {
  let logradouro = '';
  let bairro = '';
  let cep = '';
  let cidade = '';
  let uf = '';

  if (apiCorreiosResult) {
    logradouro = apiCorreiosResult.logradouro ? apiCorreiosResult.logradouro : '';
    bairro = apiCorreiosResult.bairro ? apiCorreiosResult.bairro : '';
    cep = apiCorreiosResult.cep ? apiCorreiosResult.cep : '';
    localidade = apiCorreiosResult.cidade ? apiCorreiosResult.cidade : '';
    uf = apiCorreiosResult.estado ? apiCorreiosResult.estado : '';
  }

  let cepStatus = '';
  if (databaseResult) {
    cepStatus = databaseResult.cep_status ? databaseResult.cep_status : '';
  }

  return {
    cep: cep,
    logradouro: logradouro,
    bairro: bairro,
    localidade: localidade,
    uf: uf,
    cep_status: cepStatus
  };
};

module.exports = parse;
