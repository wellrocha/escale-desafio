const config = require('../config');
const validate = require('../lib/request_validation');
const restGetRequest = require('../data/rest_get_request');
const correioQuery = require('../data/correio_query');
const parseResult = require('./parse_cep_service_result');

const hasValidationErrors = (result) => {
  return Object.getOwnPropertyNames(result).length > 0 ? true : false;
};

const service = (request) => {
  const validationResult = validate(request);
  if (hasValidationErrors(validationResult)) {
    return {
      info : 'VALIDATION_ERROR',
      errors : validationResult
    };
  }

  const cep = request.cep;
  const url = config.correio_api + cep;

  const apiResult = restGetRequest(url);
  const queryResult = correioQuery(cep);

  return Promise.all([apiResult, queryResult]).then(values => {
    return parseResult(values[0], values[1]);
  });
};

module.exports = service;
