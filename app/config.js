let environment = process.env.NODE_ENV;

const config = {
  local: {
    database: 'mongodb://localhost:27017/escale-desafio',
    correio_api: 'http://api.postmon.com.br/v1/cep/'
  },
  production: {
    database: '',
    correio_api: 'http://api.postmon.com.br/v1/cep/'
  }
};

module.exports = config[environment || 'local'];
