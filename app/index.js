const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();

module.exports = app;

mongoose.connect(config.database);

app.use(bodyParser.json());
app.use(routes);
app.listen(process.env.PORT || 3000);
