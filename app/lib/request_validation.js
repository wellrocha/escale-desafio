const validator = require('validator');
const validate = (request) => {
  let errors = {};

  const isValidCep = !request.cep ||
    !validator.isNumeric(request.cep) ||
    !validator.isLength(request.cep, {min:8, max: 8});

  if (isValidCep) {
    errors.cep = 'Cep é obrigatório, deve conter apenas números e somente 8 caracteres';
  }

  const isValidPhoneNumber = !request.phone_number ||
    !validator.isNumeric(request.phone_number) ||
    !validator.isLength(request.phone_number, {min:10, max: 11});

  if (isValidPhoneNumber) {
    errors.phone_number = 'Phone Number é obrigatório, deve conter apenas números e ter 10 ou 11 caracteres.';
  }

  return errors;
};

module.exports = validate;
