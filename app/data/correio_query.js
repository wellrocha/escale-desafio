const Correio = require('./schemas/correio.js');

const query = (cep) => {
  let filters = {};
  if (cep) {
    filters.cep = cep;
  }

  return Correio.findOne(filters).exec();
};

module.exports = query;
