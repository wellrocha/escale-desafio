const request = require('request');

module.exports = (url) => {
  return new Promise(function (resolve, reject) {
    request.get(url, {timeout: 120000}, (error, response, body) => {
      if (error) {
        return reject(error);
      }

      if (response.statusCode == 200) {
        return resolve(JSON.parse(body));
      }

      unexpected_error = new Error();
      unexpected_error.error = (`Unexpected status code: ${response.statusCode}, Url: ${url}`);

      return reject(unexpected_error);
    });
  });
};
