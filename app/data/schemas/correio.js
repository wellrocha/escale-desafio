var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var correioSchema = new Schema({
  cep: { type: Number, unique: true, trim: true },
  site: { type: String, trim: true },
  cidade: { type: String, trim: true },
  uf: { type: String, trim: true },
  cep_base: { type: Number, trim: true },
  segmentacao: { type: String, trim: true },
  area: { type: String, trim: true },
  cep_status: { type: Number, trim: true },
}, { versionKey: false });

module.exports = mongoose.model('Correio', correioSchema);
