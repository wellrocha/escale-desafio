const router = require('express').Router();
const searchCepService = require('./services/search_cep_service');

router.route('/search').get((req, res) => {
  const queryString = req.query;

  const searchResult = searchCepService(queryString);
  if (searchResult.info === 'VALIDATION_ERROR') {
    return res.status(422).json(searchResult.errors);
  }

  searchResult.then((data) => {
    return res.status(200).json(data);
  }).catch((error) => {
    return res.status(500).json(error);
  });
});

module.exports = router;
